package com.example.extandablerecycler.UnderConstruction2.CommonAdapter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.extandablerecycler.Adapter.CommonRecyclerAdapter;
import com.example.extandablerecycler.Modal.SuperClass;
import com.example.extandablerecycler.R;
import com.example.extandablerecycler.UnderConstruction.CommonRecyclerActivity2;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class CommonActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    CommonAdapter commonAdapter;
    FloatingActionButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById();
        init();
    }

    private void findViewById() {
        recyclerView = findViewById(R.id.recyclerView);
        fab = findViewById(R.id.fab);
    }

    private void init() {
        commonAdapter = new CommonAdapter(getApplicationContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(commonAdapter);

        commonAdapter.addAll(dataList());

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CommonRecyclerActivity2.class);
                startActivity(intent);
            }
        });
    }

    private List<SuperClass> dataList() {

        List<DataModal> dataModals = new ArrayList<>();
        dataModals.add(new DataModal("ME"));
        dataModals.add(new DataModal("MYSELF"));
        dataModals.add(new DataModal("YOURSELF"));
        List<SuperClass> superClasses = new ArrayList<>();
        superClasses.addAll(dataModals);
        return superClasses;
    }
}
