package com.example.extandablerecycler.UnderConstruction2.CommonAdapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.extandablerecycler.Adapter.IndividualParentAdapter;
import com.example.extandablerecycler.Modal.DataChild;
import com.example.extandablerecycler.Modal.DataParent;
import com.example.extandablerecycler.Modal.SuperClass;
import com.example.extandablerecycler.R;

import java.util.ArrayList;
import java.util.List;

public class CommonAdapter extends RecyclerView.Adapter<CommonAdapter.MyViewHolder> {
    Context context;
    List<SuperClass> superClassList = new ArrayList<>();

    public CommonAdapter(Context context) {
        this.context = context;
    }

    public void addAll(List<SuperClass> superClassList) {
        this.superClassList.clear();
        this.superClassList.addAll(superClassList);
        Log.e("SuperLIst ", String.valueOf(superClassList));
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return superClassList.size();
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View itemView = inflater.inflate(R.layout.view_category, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SuperClass dataParent = superClassList.get(position);
        holder.txtCategoryItem.setText((String) dataParent.name);

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtCategoryItem;
        ImageView imgExpand;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtCategoryItem = itemView.findViewById(R.id.txtCategoryItem);
            imgExpand = itemView.findViewById(R.id.imgExpand);

        }
    }

}