package com.example.extandablerecycler.UnderConstruction2.CommonAdapter;

import com.example.extandablerecycler.Modal.SuperClass;

public class DataModal extends SuperClass {
    public Object name;
    public boolean isSelected;
    public boolean isParent;

    public boolean isParent() {
        return isParent;
    }

    private boolean childrenVisible;

    public boolean isChildrenVisible() {
        return childrenVisible;
    }

    public void setChildrenVisible(boolean childrenVisible) {
        this.childrenVisible = childrenVisible;
    }

    public void setParent(boolean parent) {
        isParent = parent;
    }

    public DataModal() {
    }

    public DataModal(Object name) {
        this.name = name;
    }

}
