package com.example.extandablerecycler.dynamicModal;

public class DataObject {
    public Object name;
    public boolean isSelected;
    public boolean isParent;

    public boolean isParent() {
        return isParent;
    }

    private boolean childrenVisible;

    public boolean isChildrenVisible() {
        return childrenVisible;
    }

    public void setChildrenVisible(boolean childrenVisible) {
        this.childrenVisible = childrenVisible;
    }
    public void setParent(boolean parent) {
        isParent = parent;
    }

    public DataObject() {
    }

    public DataObject(Object name) {
        this.name = name;
    }

}
