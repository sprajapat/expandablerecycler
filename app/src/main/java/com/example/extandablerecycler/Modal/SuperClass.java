package com.example.extandablerecycler.Modal;

public class SuperClass {
    public boolean isParent;
    public boolean isSelected;
    public Object name;

    public boolean isParent() {
        return isParent;
    }

    public void setParent(boolean parent) {
        isParent = parent;
    }
}
