package com.example.extandablerecycler.Modal;

public class DataParent extends SuperClass {
    public Object name;
    public boolean childrenVisible;
    public boolean isSelected;

    @Override
    public boolean isParent() {
        return true;
    }

    public DataParent() {
    }

    public DataParent(Object name) {
        this.name = name;
    }

    public boolean isChildrenVisible() {
        return childrenVisible;
    }

    public void setChildrenVisible(boolean childrenVisible) {
        this.childrenVisible = childrenVisible;
    }
}
