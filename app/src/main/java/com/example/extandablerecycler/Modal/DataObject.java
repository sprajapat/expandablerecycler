package com.example.extandablerecycler.Modal;

import java.util.List;

public class DataObject {

    public int id;
    public String name;
    public String value;
    public boolean isSelected;
    public List<SubDataObject> subDataObjects;

    public DataObject() {
    }

    public DataObject(int id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public DataObject(int id, String name, List<SubDataObject> subDataObjects) {
        this.id = id;
        this.name = name;
        this.subDataObjects = subDataObjects;
    }

    public DataObject(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static class SubDataObject {
        public int id;
        public String name;
        public String value;

        public SubDataObject(int id, String name) {
            this.id = id;
            this.name = name;
        }
    }
}
