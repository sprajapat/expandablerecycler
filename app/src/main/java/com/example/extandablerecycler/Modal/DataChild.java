package com.example.extandablerecycler.Modal;

public class DataChild extends SuperClass {
    public Object name;
    public boolean isSelected;
    public boolean isParent;
    private boolean childrenVisible;

    @Override
    public boolean isParent() {
        return false;
    }

    public DataChild() {
    }

    public DataChild(Object name) {
        this.name = name;
    }
}
