package com.example.extandablerecycler.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.extandablerecycler.Modal.DataParent;
import com.example.extandablerecycler.Modal.DataChild;
import com.example.extandablerecycler.R;
import com.example.extandablerecycler.Adapter.CommonRecyclerAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class CommonRecyclerActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    CommonRecyclerAdapter commonRecyclerAdapter;
    FloatingActionButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById();
        init();
    }

    private void findViewById() {
        recyclerView = findViewById(R.id.recyclerView);
        fab = findViewById(R.id.fab);
    }

    private void init() {
        commonRecyclerAdapter = new CommonRecyclerAdapter(recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(commonRecyclerAdapter);

        commonRecyclerAdapter.addAll(dataCategoryList(), dataList());

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CommonRecyclerActivity.class);
                startActivity(intent);
            }
        });
    }

    private List<DataChild> dataList() {

        List<DataChild> dataChildren = new ArrayList<>();
        dataChildren.add(new DataChild("ME"));
        dataChildren.add(new DataChild("MYSELF"));
        dataChildren.add(new DataChild("YOURSELF"));
        return dataChildren;
    }

    private List<DataParent> dataCategoryList() {

        List<DataParent> dataObjects = new ArrayList<>();
        dataObjects.add(new DataParent("Suraj"));
        dataObjects.add(new DataParent("Raj"));
        dataObjects.add(new DataParent("MOHIT"));

        return dataObjects;
    }
}
