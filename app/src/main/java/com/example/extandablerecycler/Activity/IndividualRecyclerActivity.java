package com.example.extandablerecycler.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;

import com.example.extandablerecycler.Adapter.IndividualParentAdapter;
import com.example.extandablerecycler.Interfce.OnItemClickListener;
import com.example.extandablerecycler.Modal.DataObject;
import com.example.extandablerecycler.R;
import com.example.extandablerecycler.UnderConstruction.CommonRecyclerActivity2;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class IndividualRecyclerActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    IndividualParentAdapter parentAdapter;
    FloatingActionButton fab;
    ProgressBar progressBar;

    List<DataObject> dataObjectList = new ArrayList<>();
    List<DataObject> dataObjectList2 = new ArrayList<>();

    int iStart = 0, loadLimit = 11;
    int totalData = 0;
    int currentItems, totalItems, scrollOutItems;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById();
        init();
    }

    private void findViewById() {
        mRecyclerView = findViewById(R.id.recyclerView);
        fab = findViewById(R.id.fab);
        progressBar = findViewById(R.id.progressBar);
    }

    private void init() {
        parentAdapter = new IndividualParentAdapter(this);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(parentAdapter);

        parentAdapter.clear();
        parentAdapter.addAll(loadData());

        parentAdapter.setOnClickLisner(new OnItemClickListener() {
            @Override
            public void shortClick(View v, int position) {
                parentAdapter.changeSelection(position, true);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CommonRecyclerActivity2.class);
                startActivity(intent);
            }
        });

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = linearLayoutManager.getChildCount();
                totalItems = linearLayoutManager.getItemCount();
                scrollOutItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading && !isLastPage) {
                    if ((currentItems + scrollOutItems >= totalItems) && scrollOutItems >= 0) {
                        loadMoreData();
                    } else {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private List<DataObject> loadData() {

        for (int i = iStart; i <= loadLimit; i++) {
            dataObjectList.add(data().get(i));
        }
        totalData = dataObjectList.size();
        return dataObjectList;
    }

    private void loadMoreData() {

        progressBar.setVisibility(View.VISIBLE);
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int currentSize = totalData;
                int nextLimit = currentSize + 10;
                if (nextLimit >= data().size()) {
                    nextLimit = data().size();
                }
                dataObjectList2.clear();

                for (int i = currentSize; i < nextLimit; i++) {
                    dataObjectList2.add(data().get(i));
                }

                totalData = totalData + dataObjectList2.size();
                parentAdapter.addItems(dataObjectList2);
                parentAdapter.notifyDataSetChanged();
                isLoading = false;
                progressBar.setVisibility(View.GONE);
                dataObjectList.addAll(dataObjectList2);
            }
        }, 2000);

        if (data().size() <= dataObjectList.size()) {
            isLastPage = true;
            progressBar.setVisibility(View.GONE);
        }
    }


    private List<DataObject> data() {

        List<DataObject> dataObjects = new ArrayList<>();

        List<DataObject.SubDataObject> dataChildren = new ArrayList<>();
        dataChildren.add(new DataObject.SubDataObject(1, "ram"));
        dataChildren.add(new DataObject.SubDataObject(2, "mahesh"));
        dataChildren.add(new DataObject.SubDataObject(3, "jayesh"));

        dataObjects.add(new DataObject(1, "Suraj", dataChildren));

        List<DataObject.SubDataObject> dataChildren2 = new ArrayList<>();
        dataChildren2.add(new DataObject.SubDataObject(1, "rahul"));
        dataChildren2.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren2.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren2.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren2.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(2, "Raj", dataChildren2));

        List<DataObject.SubDataObject> dataChildren3 = new ArrayList<>();
        dataChildren3.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren3.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren3.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(3, "Rohit", dataChildren3));

        List<DataObject.SubDataObject> dataChildren4 = new ArrayList<>();
        dataChildren4.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren4.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren4.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren4.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(4, "Mukesh", dataChildren4));

        List<DataObject.SubDataObject> dataChildren5 = new ArrayList<>();
        dataChildren5.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren5.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren5.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren5.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren5.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(5, "Raj", dataChildren5));

        List<DataObject.SubDataObject> dataChildren6 = new ArrayList<>();
        dataChildren6.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren6.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren6.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(6, "Rohit", dataChildren6));

        List<DataObject.SubDataObject> dataChildren7 = new ArrayList<>();
        dataChildren7.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren7.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren7.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren7.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(7, "Mukesh", dataChildren7));


        List<DataObject.SubDataObject> dataChildren8 = new ArrayList<>();
        dataChildren8.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren8.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren8.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren8.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren8.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(8, "Raj", dataChildren8));

        List<DataObject.SubDataObject> dataChildren9 = new ArrayList<>();
        dataChildren9.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren9.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren9.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(9, "Rohit", dataChildren9));

        List<DataObject.SubDataObject> dataChildren10 = new ArrayList<>();
        dataChildren10.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren10.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren10.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren10.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(10, "Mukesh", dataChildren10));

        List<DataObject.SubDataObject> dataChildren11 = new ArrayList<>();
        dataChildren11.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren11.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren11.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren11.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren11.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(11, "Raj", dataChildren11));

        List<DataObject.SubDataObject> dataChildren12 = new ArrayList<>();
        dataChildren12.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren12.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren12.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(12, "Rohit", dataChildren12));

        List<DataObject.SubDataObject> dataChildren13 = new ArrayList<>();
        dataChildren13.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren13.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren13.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren13.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(13, "Mukesh", dataChildren13));

        List<DataObject.SubDataObject> dataChildren14 = new ArrayList<>();
        dataChildren14.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren14.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren14.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren14.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren14.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(14, "Raj", dataChildren14));

        List<DataObject.SubDataObject> dataChildren15 = new ArrayList<>();
        dataChildren15.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren15.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren15.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(15, "Rohit", dataChildren15));

        List<DataObject.SubDataObject> dataChildren16 = new ArrayList<>();
        dataChildren16.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren16.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren16.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren16.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(16, "Mukesh", dataChildren16));

        List<DataObject.SubDataObject> dataChildren17 = new ArrayList<>();
        dataChildren17.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren17.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren17.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren17.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren17.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(17, "Raj", dataChildren17));

        List<DataObject.SubDataObject> dataChildren18 = new ArrayList<>();
        dataChildren18.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren18.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren18.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(18, "Rohit", dataChildren18));

        List<DataObject.SubDataObject> dataChildren19 = new ArrayList<>();
        dataChildren4.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren4.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren4.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren4.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(19, "Mukesh", dataChildren4));

        List<DataObject.SubDataObject> dataChildren20 = new ArrayList<>();
        dataChildren20.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren20.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren20.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren20.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren20.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(20, "Raj", dataChildren20));

        List<DataObject.SubDataObject> dataChildren21 = new ArrayList<>();
        dataChildren21.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren21.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren21.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(21, "Rohit", dataChildren21));

        List<DataObject.SubDataObject> dataChildren22 = new ArrayList<>();
        dataChildren22.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren22.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren22.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren22.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(22, "Mukesh", dataChildren22));

        List<DataObject.SubDataObject> dataChildren23 = new ArrayList<>();
        dataChildren23.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren23.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren23.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren23.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren23.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(23, "Raj", dataChildren23));

        List<DataObject.SubDataObject> dataChildren24 = new ArrayList<>();
        dataChildren24.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren24.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren24.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(24, "Rohit", dataChildren24));

        List<DataObject.SubDataObject> dataChildren25 = new ArrayList<>();
        dataChildren25.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren25.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren25.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren25.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(25, "Mukesh", dataChildren25));

        List<DataObject.SubDataObject> dataChildren26 = new ArrayList<>();
        dataChildren26.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren26.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren26.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren26.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren26.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(26, "Raj", dataChildren26));

        List<DataObject.SubDataObject> dataChildren27 = new ArrayList<>();
        dataChildren27.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren27.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren27.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(27, "Rohit", dataChildren27));

        List<DataObject.SubDataObject> dataChildren29 = new ArrayList<>();
        dataChildren29.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren29.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren29.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren29.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(28, "Mukesh", dataChildren29));

        List<DataObject.SubDataObject> dataChildren30 = new ArrayList<>();
        dataChildren30.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren30.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren30.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren30.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren30.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(29, "Raj", dataChildren30));
        List<DataObject.SubDataObject> dataChildren31 = new ArrayList<>();
        dataChildren18.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren18.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren18.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(30, "Rohit", dataChildren18));

        List<DataObject.SubDataObject> dataChildren32 = new ArrayList<>();
        dataChildren32.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren32.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren32.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren32.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(31, "Mukesh", dataChildren32));

        List<DataObject.SubDataObject> dataChildren33 = new ArrayList<>();
        dataChildren33.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren33.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren33.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren33.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren33.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(32, "Raj", dataChildren33));
        List<DataObject.SubDataObject> dataChildren34 = new ArrayList<>();
        dataChildren34.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren34.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren34.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(33, "Rohit", dataChildren34));

        List<DataObject.SubDataObject> dataChildren35 = new ArrayList<>();
        dataChildren35.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren35.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren35.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren35.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(34, "Mukesh", dataChildren35));

        List<DataObject.SubDataObject> dataChildren36 = new ArrayList<>();
        dataChildren36.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren36.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren36.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren36.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren36.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(35, "Raj", dataChildren36));
        List<DataObject.SubDataObject> dataChildren37 = new ArrayList<>();
        dataChildren37.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren37.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren37.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(36, "Rohit", dataChildren37));

        List<DataObject.SubDataObject> dataChildren38 = new ArrayList<>();
        dataChildren38.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren38.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren38.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren38.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(37, "Mukesh", dataChildren38));

        List<DataObject.SubDataObject> dataChildren39 = new ArrayList<>();
        dataChildren39.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren39.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren39.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren39.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren39.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(38, "Raj", dataChildren39));
        List<DataObject.SubDataObject> dataChildren40 = new ArrayList<>();
        dataChildren40.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren40.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren40.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(39, "Rohit", dataChildren40));

        List<DataObject.SubDataObject> dataChildren41 = new ArrayList<>();
        dataChildren41.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren41.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren41.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren41.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(40, "Mukesh", dataChildren41));

        List<DataObject.SubDataObject> dataChildren42 = new ArrayList<>();
        dataChildren42.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren42.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren42.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren42.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren42.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(41, "Raj", dataChildren42));
        List<DataObject.SubDataObject> dataChildren43 = new ArrayList<>();
        dataChildren43.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren43.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren43.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(42, "Rohit", dataChildren43));

        List<DataObject.SubDataObject> dataChildren44 = new ArrayList<>();
        dataChildren44.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren44.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren44.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren44.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(43, "Mukesh", dataChildren44));

        List<DataObject.SubDataObject> dataChildren45 = new ArrayList<>();
        dataChildren45.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren45.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren45.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren45.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren45.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(44, "Raj", dataChildren45));
        List<DataObject.SubDataObject> dataChildren46 = new ArrayList<>();
        dataChildren46.add(new DataObject.SubDataObject(1, "shiv"));
        dataChildren46.add(new DataObject.SubDataObject(2, "shankar"));
        dataChildren46.add(new DataObject.SubDataObject(3, "rakesh"));

        dataObjects.add(new DataObject(45, "Rohit", dataChildren46));

        List<DataObject.SubDataObject> dataChildren47 = new ArrayList<>();
        dataChildren47.add(new DataObject.SubDataObject(1, "rajesh"));
        dataChildren47.add(new DataObject.SubDataObject(2, "rahul"));
        dataChildren47.add(new DataObject.SubDataObject(3, "sumit"));
        dataChildren47.add(new DataObject.SubDataObject(4, "suman"));

        dataObjects.add(new DataObject(46, "Mukesh", dataChildren47));

        List<DataObject.SubDataObject> dataChildren48 = new ArrayList<>();
        dataChildren48.add(new DataObject.SubDataObject(1, "rohit"));
        dataChildren48.add(new DataObject.SubDataObject(2, "pinkesh"));
        dataChildren48.add(new DataObject.SubDataObject(3, "punit"));
        dataChildren48.add(new DataObject.SubDataObject(4, "pt"));
        dataChildren48.add(new DataObject.SubDataObject(5, "pulkit"));

        dataObjects.add(new DataObject(47, "Raj", dataChildren48));
        return dataObjects;
    }
}
