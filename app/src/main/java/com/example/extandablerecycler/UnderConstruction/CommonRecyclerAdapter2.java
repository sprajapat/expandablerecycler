package com.example.extandablerecycler.UnderConstruction;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.extandablerecycler.Interfce.OnItemClickListener;
import com.example.extandablerecycler.Modal.DataChild;
import com.example.extandablerecycler.Modal.DataParent;
import com.example.extandablerecycler.Modal.SuperClass;
import com.example.extandablerecycler.R;

import java.util.ArrayList;
import java.util.List;

public class CommonRecyclerAdapter2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    CommonRecyclerAdapter2 commonRecyclerAdapter2;
    RecyclerView recyclerView;
    OnItemClickListener monClickLisner;

    List<SuperClass> superClassList = new ArrayList<>();
    List<DataParent> parentList = new ArrayList<>();
    List<DataChild> childList = new ArrayList<>();

    private static final int PARENT = 1;
    private static final int CHILD = 0;

    public CommonRecyclerAdapter2(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        commonRecyclerAdapter2 = this;
        setParent();
    }

    public void setParent() {
        for (int x = 0; x < parentList.size(); x++) {
            DataParent person = parentList.get(x);
            person.setChildrenVisible(false);
        }
    }

    public CommonRecyclerAdapter2(Context context) {
        this.context = context;
    }

    public void addAll(List<DataParent> categoryList, List<DataChild> dataList) {
        this.parentList.clear();
        this.parentList.addAll(categoryList);
        superClassList.addAll(categoryList);
        this.childList.clear();
        this.childList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (superClassList.size() != 0) {
            if (superClassList.get(position).isParent()) {
                return PARENT;
            } else {
                return CHILD;
            }
        } else {
            return 0;
        }
    }

    @Override
    public int getItemCount() {
        return superClassList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case PARENT:
                View itemView = inflater.inflate(R.layout.view_category, parent, false);
                return new ParentViewHolder(itemView);
            case CHILD:
                itemView = inflater.inflate(R.layout.view_data, parent, false);
                return new ChildViewHolder(itemView);
            default:
                throw new IllegalArgumentException("viewType is not valid");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder.getItemViewType() == CHILD) {
            DataChild dataChild = (DataChild) superClassList.get(position);
            TextView txtData = ((ChildViewHolder) holder).txtData;
            txtData.setText((String) dataChild.name);

        } else {
            final DataParent dataParent = (DataParent) superClassList.get(position);
            TextView txtCategoryItem = ((ParentViewHolder) holder).txtCategoryItem;
            LinearLayout llParent = ((ParentViewHolder) holder).llParent;
            final ImageView imgExpand = ((ParentViewHolder) holder).imgExpand;
            txtCategoryItem.setText((String) dataParent.name);

            llParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    monClickLisner.shortClick(v, position);


                    if (!dataParent.childrenVisible) {
                        imgExpand.setImageResource(R.drawable.ic_collapse);
                        expandList(position, dataParent);
                    } else {
                        imgExpand.setImageResource(R.drawable.ic_expand);
                        collapseList(position, dataParent);
                    }
                }
            });
        }
    }

    private void expandList(int id, DataParent parent) {
        int k=id;
        if (childList.size() != 0) {
            for (int i = 0; i < superClassList.size(); i++) {
                if (i != id) {
                    DataParent dataParent = (DataParent) superClassList.get(i);

                    if (dataParent.isChildrenVisible()) {
                        dataParent.setChildrenVisible(false);
                        for (int j = i + 1; j < (i + 1 + childList.size()); j++) {
                            superClassList.remove(i + 1);
                        k = i + 1;
                        }
                        notifyDataSetChanged();
                        commonRecyclerAdapter2.notifyItemRangeRemoved(i + 1, childList.size());

                        int lastVisibleItemPosition = ((LinearLayoutManager) commonRecyclerAdapter2.recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                        if ((i + 1) < superClassList.size()) {
                            if ((i + 1) >= lastVisibleItemPosition) {
                                commonRecyclerAdapter2.recyclerView.scrollToPosition(i + 1);
                            }
                        }
                    }
                }
            }

            parent.setChildrenVisible(true);
            int index = 0;
            for (int i = k + 1; i < (k + 1 + childList.size()); i++) {
                superClassList.add(i, (SuperClass) childList.get(index));
                index++;
            }
            notifyDataSetChanged();
            Log.e("added items after ", k + " " + superClassList);
            commonRecyclerAdapter2.notifyItemRangeInserted(k + 1, childList.size());

            int lastVisibleItemPosition = ((LinearLayoutManager) commonRecyclerAdapter2.recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            if ((k + 1) < superClassList.size()) {
                if ((k + 1) >= lastVisibleItemPosition) {
                    commonRecyclerAdapter2.recyclerView.scrollToPosition(k + 1);
                }
            }
        }
    }

    private void collapseList(int id, DataParent dataParent) {
        if (childList.size() != 0) {
            if (dataParent.isChildrenVisible()) {
                dataParent.setChildrenVisible(false);
                for (int i = id + 1; i < (id + 1 + childList.size()); i++) {
                    superClassList.remove(id + 1);
                }

                notifyDataSetChanged();
                commonRecyclerAdapter2.notifyItemRangeRemoved(id + 1, childList.size());
                Log.e("removed items after ", id + " " + superClassList);

                int lastVisibleItemPosition = ((LinearLayoutManager) commonRecyclerAdapter2.recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                if ((id + 1) < superClassList.size()) {
                    if ((id + 1) >= lastVisibleItemPosition) {
                        commonRecyclerAdapter2.recyclerView.scrollToPosition(id + 1);
                    }
                }
            }
        }
    }


    //parentview holder
    private class ParentViewHolder extends RecyclerView.ViewHolder {
        TextView txtCategoryItem;
        ImageView imgExpand;
        LinearLayout llParent;

        public ParentViewHolder(View itemView) {
            super(itemView);
            txtCategoryItem = itemView.findViewById(R.id.txtCategoryItem);
            imgExpand = itemView.findViewById(R.id.imgExpand);
            llParent = itemView.findViewById(R.id.llParent);
        }
    }

    //childview holder
    private class ChildViewHolder extends RecyclerView.ViewHolder {
        TextView txtData;

        public ChildViewHolder(View itemView) {
            super(itemView);
            txtData = itemView.findViewById(R.id.txtData);
        }
    }

    public void changeSelection(int position, boolean singleselect) {
        Log.e("superList items are", " " + superClassList);
        if (singleselect) {
            for (int i = 0; i < superClassList.size(); i++) {
                if (position == i) {
                    superClassList.get(i).isSelected = true;
                } else {
                    superClassList.get(i).isSelected = false;
                }
            }
        } else {

            superClassList.get(position).isSelected = !superClassList.get(position).isSelected;
        }
//        notifyDataSetChanged();
    }

    public void setOnClickLisner(OnItemClickListener onClickLisner) {
        monClickLisner = onClickLisner;
    }
}