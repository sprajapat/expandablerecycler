package com.example.extandablerecycler.UnderConstruction;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.extandablerecycler.Interfce.OnItemClickListener;
import com.example.extandablerecycler.Modal.DataChild;
import com.example.extandablerecycler.Modal.DataParent;
import com.example.extandablerecycler.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class CommonRecyclerActivity2 extends AppCompatActivity {
    RecyclerView recyclerView;
    CommonRecyclerAdapter2 commonRecyclerAdapter2;
    FloatingActionButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById();
        init();
    }

    private void findViewById() {
        recyclerView = findViewById(R.id.recyclerView);
        fab = findViewById(R.id.fab);
    }

    private void init() {
        commonRecyclerAdapter2 = new CommonRecyclerAdapter2(recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(commonRecyclerAdapter2);

        commonRecyclerAdapter2.addAll(dataCategoryList(), dataList());

        commonRecyclerAdapter2.setOnClickLisner(new OnItemClickListener() {
            @Override
            public void shortClick(View v, int position) {
//                commonRecyclerAdapter2.changeSelection(position, true);
            }
        });

    }

    private List<DataChild> dataList() {

        List<DataChild> dataChildren = new ArrayList<>();
        dataChildren.add(new DataChild("ME"));
        dataChildren.add(new DataChild("MYSELF"));
        dataChildren.add(new DataChild("YOURSELF"));
        return dataChildren;
    }

    private List<DataParent> dataCategoryList() {

        List<DataParent> dataObjects = new ArrayList<>();
        dataObjects.add(new DataParent("Suraj"));
        dataObjects.add(new DataParent("Raj"));
        dataObjects.add(new DataParent("MOHIT"));
        dataObjects.add(new DataParent("MOHIT"));

        return dataObjects;
    }
}
