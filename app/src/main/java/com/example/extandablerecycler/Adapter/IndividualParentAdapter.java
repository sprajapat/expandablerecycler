package com.example.extandablerecycler.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.extandablerecycler.Interfce.OnItemClickListener;
import com.example.extandablerecycler.Modal.DataObject;
import com.example.extandablerecycler.R;

import java.util.ArrayList;
import java.util.List;

public class IndividualParentAdapter extends RecyclerView.Adapter<IndividualParentAdapter.MyViewHolder> implements Filterable {
    Context context;
    private Filter fRecords;
    OnItemClickListener mOnClickLisner;
    List<DataObject> categoryList = new ArrayList<>();

    public IndividualParentAdapter(Context context) {
        this.context = context;
    }

    public void addAll(List<DataObject> categoryList) {
//        this.categoryList.clear();
        this.categoryList.addAll(categoryList);
        notifyDataSetChanged();
    }

    public void addItems(List<DataObject> categoryList) {
        int i = this.categoryList.size();
        this.categoryList.addAll(categoryList);
        notifyItemInserted(i+1);
//        notifyDataSetChanged();
    }

    public void clear() {
        this.categoryList.clear();
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View itemView = inflater.inflate(R.layout.view_category, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        DataObject dataParent = categoryList.get(position);
        holder.txtId.setText(String.valueOf(dataParent.id));
        holder.txtCategoryItem.setText(dataParent.name);

        IndividualChildAdapter childAdapter = new IndividualChildAdapter(context);

        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.recyclerView.setAdapter(childAdapter);
        childAdapter.addAll(dataParent.subDataObjects);

        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnClickLisner.shortClick(v, position);

            }
        });

        if (dataParent.isSelected) {
            holder.recyclerView.setVisibility(View.VISIBLE);
        } else {
            holder.recyclerView.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public void changeSelection(int position, boolean singleselect) {
        if (singleselect) {
            for (int i = 0; i < categoryList.size(); i++) {
                if (position == i) {
                    categoryList.get(i).isSelected = true;
                } else {
                    categoryList.get(i).isSelected = false;
                }
            }
        } else {
            categoryList.get(position).isSelected = !categoryList.get(position).isSelected;
        }
        notifyDataSetChanged();
    }

    public void setOnClickLisner(OnItemClickListener onClickLisner) {
        mOnClickLisner = onClickLisner;
    }

    @Override
    public Filter getFilter() {
        if (fRecords == null) {
            fRecords = new RecordFilter();
        }
        return fRecords;
    }

    private class RecordFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint == null || constraint.length() == 0) {
                //No need for filter
                results.values = categoryList;
                results.count = categoryList.size();

            } else {
                //Need Filter
                // it matches the text  entered in the edittext and set the data in adapter list
                ArrayList<DataObject> fRecords = new ArrayList<>();

                for (DataObject s : categoryList) {
                    fRecords.add(s);
                }
                results.values = fRecords;
                results.count = fRecords.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            //it set the data from filter to adapter list and refresh the recyclerview adapter
            categoryList = (ArrayList<DataObject>) results.values;
            notifyDataSetChanged();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtCategoryItem, txtId;
        LinearLayout llParent;
        RecyclerView recyclerView;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtCategoryItem = itemView.findViewById(R.id.txtCategoryItem);
            txtId = itemView.findViewById(R.id.txtId);
            llParent = itemView.findViewById(R.id.llParent);
            recyclerView = itemView.findViewById(R.id.recyclerView);
        }
    }
}
