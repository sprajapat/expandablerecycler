package com.example.extandablerecycler.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.extandablerecycler.Interfce.OnItemClickListener;
import com.example.extandablerecycler.Modal.DataObject;
import com.example.extandablerecycler.R;

import java.util.ArrayList;
import java.util.List;

public class IndividualChildAdapter extends RecyclerView.Adapter<IndividualChildAdapter.MyViewHolder> {
    Context context;
    List<DataObject.SubDataObject> subDataObjects = new ArrayList<>();
    OnItemClickListener mOnClickLisner;


    public IndividualChildAdapter(Context context) {
        this.context = context;
    }


    public void addAll(List<DataObject.SubDataObject> subDataObjects) {
        this.subDataObjects.clear();
        this.subDataObjects.addAll(subDataObjects);
        notifyDataSetChanged();
    }


    public void setOnClickLisner(OnItemClickListener onClickLisner) {
        mOnClickLisner = onClickLisner;
    }

    @Override
    public IndividualChildAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.view_data, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull IndividualChildAdapter.MyViewHolder holder, int position) {
        DataObject.SubDataObject dataChild = subDataObjects.get(position);
        holder.txtData.setText((String) dataChild.name);
    }

    @Override
    public int getItemCount() {
        return subDataObjects.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtData;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtData = itemView.findViewById(R.id.txtData);

        }
    }
}