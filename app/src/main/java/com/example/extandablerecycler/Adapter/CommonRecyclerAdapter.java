package com.example.extandablerecycler.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.extandablerecycler.Modal.DataParent;
import com.example.extandablerecycler.Modal.DataChild;
import com.example.extandablerecycler.Modal.SuperClass;
import com.example.extandablerecycler.R;

import java.util.ArrayList;
import java.util.List;

public class CommonRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    CommonRecyclerAdapter commonRecyclerAdapter;
    RecyclerView recyclerView;


    List<SuperClass> superClassList = new ArrayList<>();
    List<DataParent> parentList = new ArrayList<>();
    List<DataChild> childList = new ArrayList<>();

    private static final int PARENT = 1;
    private static final int CHILD = 0;

    public CommonRecyclerAdapter(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        commonRecyclerAdapter = this;
        setParent();
    }

    public void setParent() {
        for (int x = 0; x < parentList.size(); x++) {
            DataParent person = parentList.get(x);
            person.setChildrenVisible(false);
        }
    }

    public CommonRecyclerAdapter(Context context) {
        this.context = context;
    }

    public void addAll(List<DataParent> categoryList, List<DataChild> dataList) {
        this.parentList.clear();
        this.parentList.addAll(categoryList);
        superClassList.addAll(categoryList);
        this.childList.clear();
        this.childList.addAll(dataList);
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position) {
        Log.e("entityList items are ", String.valueOf(superClassList.get(position)));
        if (superClassList.size() != 0) {
            if (superClassList.get(position).isParent()) {
                Log.e("viewtype = ", "parent at " + position);
                return PARENT;
            } else {
                return CHILD;
            }
        } else {
            Log.e("empty list", " " + position);
            return 0;
        }
    }

    @Override
    public int getItemCount() {
        return superClassList.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case PARENT:
                View itemView = inflater.inflate(R.layout.view_category, parent, false);
                return new ParentViewHolder(itemView);
            case CHILD:
                itemView = inflater.inflate(R.layout.view_data, parent, false);
                return new ChildViewHolder(itemView);
            default:
                throw new IllegalArgumentException("viewType is not valid");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder.getItemViewType() == CHILD) {
            DataChild dataChild = (DataChild) superClassList.get(position);
            TextView txtData = ((ChildViewHolder) holder).txtData;
            txtData.setText((String) dataChild.name);


        } else {
            DataParent dataParent = (DataParent) superClassList.get(position);
            TextView txtCategoryItem = ((ParentViewHolder) holder).txtCategoryItem;
            txtCategoryItem.setText((String) dataParent.name);
        }

    }


    private class ParentViewHolder extends RecyclerView.ViewHolder {
        TextView txtCategoryItem;


        public ParentViewHolder(View itemView) {
            super(itemView);
            txtCategoryItem = itemView.findViewById(R.id.txtCategoryItem);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id = getLayoutPosition();

                    DataParent person = (DataParent) superClassList.get(id);
                    if (childList.size() != 0) {
                        //collapse list
                        if (person.isChildrenVisible()) {
//                            person.isSelected = !person.isSelected;
                            person.setChildrenVisible(false);

                            for (int i = id + 1; i < (id + 1 + childList.size()); i++) {
                                superClassList.remove(id + 1);
                            }
                            commonRecyclerAdapter.notifyItemRangeRemoved(id + 1, childList.size());
                        }
                        //expand list
                        else {
                            person.setChildrenVisible(true);
                            int index = 0;

                            for (int i = id + 1; i < (id + 1 + childList.size()); i++) {
                                superClassList.add(i, (SuperClass) childList.get(index));
                                index++;
                            }

                            commonRecyclerAdapter.notifyItemRangeInserted(id + 1, childList.size());
                        }

                        int lastVisibleItemPosition = ((LinearLayoutManager) commonRecyclerAdapter.recyclerView.
                                getLayoutManager()).findLastCompletelyVisibleItemPosition();
                        if ((id + 1) < superClassList.size()) {
                            if ((id + 1) >= lastVisibleItemPosition) {
                                commonRecyclerAdapter.recyclerView.scrollToPosition(id + 1);
                            }
                        }
                    }
                }
            });
        }
    }

    private class ChildViewHolder extends RecyclerView.ViewHolder {
        TextView txtData;

        public ChildViewHolder(View itemView) {
            super(itemView);
            txtData = itemView.findViewById(R.id.txtData);
        }
    }
}
